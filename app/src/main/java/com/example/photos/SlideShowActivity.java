package com.example.photos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.ImageDecoder;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

public class SlideShowActivity extends AppCompatActivity {

    ArrayList<Album> albums;
    ArrayList<Photo> albumPhotos;
    ImageView photoImage;
    int selectedAlbum;
    TextView leftArrowTextView;
    TextView rightArrowTextView;

    int photoIndex;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.slideshow);
        albums = (ArrayList<Album>)getIntent().getSerializableExtra("albums");
        albumPhotos = (ArrayList<Photo>)getIntent().getSerializableExtra("photos");
        selectedAlbum = getIntent().getIntExtra("selectedPos", 0);
        photoIndex = 0;
        photoImage = findViewById(R.id.photoImage);
        //write first image
        File imageFile = new File(albumPhotos.get(0).filePath);
        ImageDecoding id = new ImageDecoding(imageFile);
        photoImage.setImageDrawable(id.draw);
        leftArrowTextView = findViewById(R.id.leftArrowTextView);
        leftArrowTextView.setOnClickListener((v)->goLeft());
        rightArrowTextView = findViewById(R.id.rightArrowTextView);
        rightArrowTextView.setOnClickListener((v)->goRight());
        leftArrowTextView.setVisibility(View.INVISIBLE);
        if(albumPhotos.size() == 1){
            rightArrowTextView.setVisibility(View.INVISIBLE);
        }
    }

    private void goRight(){
        ++photoIndex;
        leftArrowTextView.setVisibility(View.VISIBLE);
        if(photoIndex == albumPhotos.size()-1){
            rightArrowTextView.setVisibility(View.INVISIBLE);
        }
        //write new image
        File imageFile = new File(albumPhotos.get(photoIndex).filePath);
        ImageDecoding id = new ImageDecoding(imageFile);
        photoImage.setImageDrawable(id.draw);
    }

    private void goLeft(){
        --photoIndex;
        rightArrowTextView.setVisibility(View.VISIBLE);
        if(photoIndex == 0){
            leftArrowTextView.setVisibility(View.INVISIBLE);
        }
        //write new image
        File imageFile = new File(albumPhotos.get(photoIndex).filePath);
        ImageDecoding id = new ImageDecoding(imageFile);
        photoImage.setImageDrawable(id.draw);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(getApplicationContext(), PhotoViewingActivity.class);
        intent.putExtra("albums", albums);
        intent.putExtra("photos", albumPhotos);
        intent.putExtra("selectedPos", selectedAlbum);
        startActivity(intent);
        return true;
    }
}
