package com.example.photos;

import android.graphics.ImageDecoder;
import android.graphics.drawable.Drawable;

import java.io.File;
import java.io.IOException;

public class ImageDecoding {
    Drawable draw;
    public ImageDecoding(File imageFile){
        draw = getDrawable(imageFile);
    }
    public Drawable getDrawable(File imageFile){
        ImageDecoder.Source source = ImageDecoder.createSource(imageFile);
        try {
            return ImageDecoder.decodeDrawable(source);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
