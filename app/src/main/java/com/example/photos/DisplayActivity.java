package com.example.photos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class DisplayActivity extends AppCompatActivity implements DisplayActivityDialog.AddTagListener, DisplayActivityDialog.DeleteTagListener {

    ArrayList<Photo> photos;
    ArrayList<Album> albums;
    ArrayList<Tag> tags;
    int selectedAlbum;
    int selectedPhoto;
    ImageView photoImage;
    ListView photoTags;
    Button addTagBtn;
    Button deleteTagBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display);
        photos = (ArrayList<Photo>)getIntent().getSerializableExtra("photos");
        albums = (ArrayList<Album>)getIntent().getSerializableExtra("albums");
        photoTags = findViewById(R.id.photoTagsListView);
        addTagBtn = findViewById(R.id.addTagBtn);
        addTagBtn.setOnClickListener((v)->addTag());
        deleteTagBtn = findViewById(R.id.deleteTagBtn);
        deleteTagBtn.setOnClickListener((v)->deleteTag());
        selectedAlbum = getIntent().getIntExtra("selectedPos", 0);
        selectedPhoto = getIntent().getIntExtra("selected photo", 0);
        tags = albums.get(selectedAlbum).photos.get(selectedPhoto).tags;
        ArrayAdapter<Tag> aA = new ArrayAdapter<>(this, R.layout.tags, photos.get(selectedPhoto).tags);
        photoTags.setAdapter(aA);
        photoImage = findViewById(R.id.photoImage);
        ImageDecoding id = new ImageDecoding(new File(photos.get(selectedPhoto).filePath));
        photoImage.setImageDrawable(id.draw);
    }

    private void deleteTag(){
        Bundle bundle = new Bundle();
        bundle.putString("operation", "delete tag");
        AppCompatDialogFragment frag = new DisplayActivityDialog();
        frag.setArguments(bundle);
        frag.show(getSupportFragmentManager(), "Delete Tag");
    }

    private void addTag(){
        Bundle bundle = new Bundle();
        bundle.putString("operation", "add tag");
        AppCompatDialogFragment frag = new DisplayActivityDialog();
        frag.setArguments(bundle);
        frag.show(getSupportFragmentManager(), "Add Tag");
    }

    @Override
    public void deleteTag(Tag t) {
        if(tags.contains(t)){
            tags.remove(t);
        }
        ArrayAdapter<Tag> aA = new ArrayAdapter<>(this, R.layout.tags, albums.get(selectedAlbum).photos.get(selectedPhoto).tags);
        photoTags.setAdapter(aA);
    }

    @Override
    public void addTag(Tag t) {
        if(!tags.contains(t)){
            tags.add(t);
        }
        ArrayAdapter<Tag> aA = new ArrayAdapter<>(this, R.layout.tags, albums.get(selectedAlbum).photos.get(selectedPhoto).tags);
        photoTags.setAdapter(aA);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(getApplicationContext(), PhotoViewingActivity.class);
        intent.putExtra("albums", albums);
        intent.putExtra("photos", photos);
        intent.putExtra("selectedPos", selectedAlbum);
        startActivity(intent);
        return true;
    }

    @Override
    protected void onStop(){
        super.onStop();
        FileOutputStream fos = null;
        try {
            fos = openFileOutput("user.dat", MODE_PRIVATE);
        } catch (FileNotFoundException e) {
            File file = new File(getFilesDir(), "user.dat");
            try {
                file.createNewFile();
            } catch (IOException e1) {

            }
        }
        ObjectOutputStream oos = null;
        try{
            oos = new ObjectOutputStream(fos);
            oos.writeObject(albums);
        } catch(Exception e){
            return;
        }
    }
}
