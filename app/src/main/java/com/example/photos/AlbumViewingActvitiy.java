package com.example.photos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class AlbumViewingActvitiy extends AppCompatActivity implements AlbumActivityDialog.AddAlbumDialogListener, AlbumActivityDialog.DeleteAlbumDialogListener,
AlbumActivityDialog.RenameAlbumListener{

    private static final String USERDATA = "user.dat";
    private ArrayList<Album> allAlbums;
    private ListView albums;
    private ArrayAdapter<Album> aAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button addAlbumBtn = findViewById(R.id.addAlbumBtn);
        getSupportActionBar().setTitle("Home");
        addAlbumBtn.setOnClickListener(v -> addAlbum());
        Button deleteAlbumBtn = findViewById(R.id.deleteAlbumBtn);
        deleteAlbumBtn.setOnClickListener(v -> deleteAlbum());
        Button renameAlbumBtn = findViewById(R.id.renameAlbumBtn);
        renameAlbumBtn.setOnClickListener(v -> renameAlbum());
        Button globalSearchBtn = findViewById(R.id.globalPhotoSearchBtn);
        globalSearchBtn.setOnClickListener((v)->search());
        albums = (ListView)findViewById(R.id.albumsListView);
        if(getIntent().hasExtra("albums")){
            //another activity modified information in user's albums, reinitialize to account for this
            allAlbums = (ArrayList<Album>)getIntent().getSerializableExtra("albums");
        } else{
            //load from user.dat
            FileInputStream fis = null;
            try {
                fis = openFileInput("user.dat");
            } catch (FileNotFoundException e) {
                File file = new File(getFilesDir(), "user.dat");
                try {
                    file.createNewFile();
                } catch (IOException e1) {
                }
                for(File f : getFilesDir().listFiles()){
                    f.getName();
                }
            }
            ObjectInputStream ois = null;
            try {
                ois = new ObjectInputStream(fis);
                allAlbums = (ArrayList<Album>)ois.readObject();
            } catch (Exception e) {
                allAlbums = new ArrayList<Album>();
            }
        }
        if(getIntent().hasExtra("new album")){
            Album toAdd = (Album)getIntent().getSerializableExtra("new album");
            if(!allAlbums.contains(toAdd)){
                allAlbums.add(toAdd);
            }
        }
        aAdapter = new ArrayAdapter<Album>(this, R.layout.albums, allAlbums);
        albums.setAdapter(aAdapter);
        albums.setOnItemClickListener((p, v, pos, id)->albumClick(pos));
    }

    private void search(){
        Intent intent = new Intent(getApplicationContext(), Search.class);
        intent.putExtra("albums", allAlbums);
        startActivity(intent);
    }

    protected void onStop(){
        super.onStop();
        FileOutputStream fos = null;
        try {
            fos = openFileOutput("user.dat", MODE_PRIVATE);
        } catch (FileNotFoundException e) {
            File file = new File(getFilesDir(), "user.dat");
            try {
                file.createNewFile();
            } catch (IOException e1) {

            }
        }
        ObjectOutputStream oos = null;
        try{
            oos = new ObjectOutputStream(fos);
            oos.writeObject(allAlbums);
        } catch(Exception e){
            return;
        }
    }

    private void addAlbum(){
        AppCompatDialogFragment addAlbumDialog = new AlbumActivityDialog();
        Bundle args = new Bundle();
        args.putString("operation", "add album");
        addAlbumDialog.setArguments(args);
        addAlbumDialog.show(getSupportFragmentManager(), "Input Album Name");
    }

    private void deleteAlbum(){
        AppCompatDialogFragment deleteAlbumDialog = new AlbumActivityDialog();
        Bundle args = new Bundle();
        args.putString("operation", "delete album");
        deleteAlbumDialog.setArguments(args);
        deleteAlbumDialog.show(getSupportFragmentManager(), "Input Album Name");
    }

    private void renameAlbum(){
        AppCompatDialogFragment renameAlbumDialog = new AlbumActivityDialog();
        Bundle args = new Bundle();
        args.putString("operation", "rename album");
        renameAlbumDialog.setArguments(args);
        renameAlbumDialog.show(getSupportFragmentManager(), "Input Album Name");
    }

    private void albumClick(int pos){
        //launch photo viewing activity and send data for photos of album over
        Intent intent = new Intent(getApplicationContext(), PhotoViewingActivity.class);
        intent.putExtra("albums", allAlbums);
        intent.putExtra("selectedPos", pos);
        startActivity(intent);
    }

    @Override
    public void addAlbum(Album a) {
        if(!allAlbums.contains(a)){
            allAlbums.add(a);
        }
        albums.setAdapter(new ArrayAdapter<Album> (this, R.layout.albums, allAlbums));
    }

    @Override
    public void deleteAlbum(Album a) {
        if(allAlbums.contains(a)){
            allAlbums.remove(a);
        }
        albums.setAdapter(new ArrayAdapter<Album> (this, R.layout.albums, allAlbums));
    }

    @Override
    public void renameAlbum(Album toRename, String newName) {
        if(allAlbums.contains(toRename)){
            int i = allAlbums.indexOf(toRename);
            if(!allAlbums.contains(new Album(newName))){
                allAlbums.get(i).name = newName;
            }
            albums.setAdapter(new ArrayAdapter<Album> (this, R.layout.albums, allAlbums));
        }
    }
}
