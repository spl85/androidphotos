/**
 * @author Stefan Lovric and Barry Langella
 */

package com.example.photos;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/** Album class aggregates a user's photos */
public class Album implements Serializable {

    /** Photos in the album */
    public ArrayList<Photo> photos;

    /** Name of the album */
    public String name;

    /** Earliest date of photo in album */
    public Date earliestPhotoDate;

    /** Latest date of photo in album */
    public Date latestPhotoDate;

    /** Number of photos in the album */
    public int numPhotos;

    /**
     * Creates a new Album object with a name and empty photo set
     * @param name Name of the album
     */
    public Album(String name){
        this.name = name;
        photos = new ArrayList<Photo>();
        earliestPhotoDate = null;
        latestPhotoDate = null;
        numPhotos = 0;
    }

    /**
     * Creates a new Album object with a name and specified photo set
     * @param name Starting list of photos
     * @param photos List of photos to initially add to album
     */
    public Album(String name, ArrayList<Photo> photos){
        this.name = name;
        if(photos == null){
            this.photos = new ArrayList<Photo>();
        } else {
            addAllPhotos(photos);
        }
    }

    /**
     * Add a photo to the album
     * @param p Photo to add
     * @return Returns true if add was successful, otherwise returns false
     */
    public boolean addPhoto(Photo p){
        if(this.photos.contains(p)) return false;
        photos.add(p);
        ++numPhotos;
        return true;
    }

    /**
     * Add a number of photos to the album (will append to current list)
     * @param photos List of photos to add to album
     */
    public void addAllPhotos(ArrayList<Photo> photos){
        for(Photo p : photos){
            addPhoto(p);
        }
    }

    /**
     * Gets all current photos in
     * @return ArrayList of photos currently in the album
     */
    public ArrayList<Photo> getAllPhotos(){
        return this.photos;
    }

    /**
     * Removes a photo from this album
     * @param p Photo to remove
     * @return True if the remove was successful, otherwise returns false
     */
    public boolean removePhoto(Photo p){
        if(this.photos.remove(p)){
            --numPhotos;
            return true;
        } else{
            return false;
        }
    }

    /**
     * Removes all the photos currently in the album
     */
    public void removeAllPhotos(){
        for(Photo p : this.photos){
            removePhoto(p);
        }
    }

    /**
     * Removes a set of photos pointed to by photos
     * @param photos Array List of photos to remove
     */
    public void removeSelectPhotos(ArrayList<Photo> photos){
        for(Photo p : photos){
            removePhoto(p);
        }
    }

    /**
     * Performs a single tag search through this album
     * @param t Tag to search for in photo
     * @return List of photos that have tag t in this album
     */
    public ArrayList<Photo> singleTagSearch(Tag t){
        ArrayList<Photo> ret = new ArrayList<Photo>();
        for(Photo p : this.photos){
            if(p.hasTag(t)){
                ret.add(p);
            }
        }
        return ret;
    }

    /**
     * Performs a double tag search that can be either conjunctive or disjunctive
     * @param t1 First tag parameter
     * @param t2 Second tag parameter
     * @param mode Mode is "and" for conjunctive, and "or" for disjunctive
     * @return List of photos that have tag t1 and/or tag t2
     */
    public ArrayList<Photo> doubleTagSearch(Tag t1, Tag t2, String mode){
        if(t1 == null || t2 == null || mode == null) return new ArrayList<Photo>();
        ArrayList<Photo> ret = new ArrayList<Photo>();
        if(mode.equalsIgnoreCase("and")){
            for(Photo p : this.photos){
                if(p.hasTag(t1) && p.hasTag(t2)){
                    ret.add(p);
                }
            }
        }
        if(mode.equalsIgnoreCase("or")){
            ret.addAll(singleTagSearch(t1));
            ret.addAll(singleTagSearch(t2));
        }
        return ret;
    }

    /**
     * Turns the album into a readable, and displayable string
     * @return Representation of an Album in String format
     */
    public String toString(){
        return this.name;
    }

    /**
     * Checks for equality between an object and this album
     * @param o Object to be compared
     * @return  True if albums are the same; false otherwise.
     */
    public boolean equals(Object o){
        if(!(o instanceof Album)) return false;

        Album toCompare = (Album)o;

        if(toCompare.name.equals(this.name)) return true;
        return false;
    }
}
