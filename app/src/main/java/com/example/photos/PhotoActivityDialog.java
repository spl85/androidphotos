package com.example.photos;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;

public class PhotoActivityDialog extends AppCompatDialogFragment {
    String operation;
    ListView albumsToMoveToListView;
    EditText photoPath;
    EditText photoName;
    EditText photoToMoveName;
    MovePhotoDialogListener moveListener;
    AddPhotoDialogListener addListener;
    DeletePhotoDialogListener deleteListener;
    ArrayList<Album> currentAlbums;
    Album thisAlbum;
    int selectedAlbum;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        moveListener = (MovePhotoDialogListener)context;
        addListener = (AddPhotoDialogListener)context;
        deleteListener = (DeletePhotoDialogListener)context;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if(getArguments() != null){
            operation = getArguments().getString("operation");
            ArrayList<Album> allAlbums = new ArrayList<Album>();
            allAlbums.addAll((ArrayList<Album>)getArguments().getSerializable("albums"));
            selectedAlbum = getArguments().getInt("selected album");
            thisAlbum = allAlbums.get(selectedAlbum);
            if(operation.equals("move photo"))
            allAlbums.remove(selectedAlbum);
            currentAlbums = allAlbums;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = null;
        if(operation.equals("add photo")){
            view = inflater.inflate(R.layout.add_photo_dialog, null);
            photoPath = view.findViewById(R.id.photoPathEditText);
            builder.setPositiveButton("ok", ((dialog, which) -> okClick()));
            builder.setTitle("Input Photo Path");
        }
        if(operation.equals("delete photo")){
            view = inflater.inflate(R.layout.delete_photo_dialog, null);
            photoName = view.findViewById(R.id.photoNameEditText);
            builder.setPositiveButton("ok", ((dialog, which) -> okClick()));
            builder.setTitle("Input Photo Name");
        }
        if(operation.equals("move photo")){
            view = inflater.inflate(R.layout.move_photo_dialog, null);
            builder.setTitle("Select Album to Move to");
            photoToMoveName = view.findViewById(R.id.photoToMoveNameEditText);
            albumsToMoveToListView = view.findViewById(R.id.albumsToMoveToListView);
            albumsToMoveToListView.setAdapter(new ArrayAdapter<Album>(getActivity().getApplicationContext(), R.layout.albums, currentAlbums));
            albumsToMoveToListView.setOnItemClickListener((parent, view1, position, id)->albumChoiceClick(position));
        }
        builder.setView(view);
        builder.setNegativeButton("cancel", (dialog, which) -> {});
        return builder.create();
    }

    private void okClick(){
        //operation was either add photo or delete photo
        if(operation.equals("add photo")){
            File file = new File(photoPath.getText().toString());
            addListener.addPhoto(new Photo(file.getPath(), file.getName()));
        }
        if(operation.equals("delete photo")){
            Photo p = new Photo(photoName.getText().toString());
            deleteListener.deletePhoto(p);
        }

    }

    private void albumChoiceClick(long albumToMoveToPosition){
        String photoName = photoToMoveName.getText().toString();
        if(photoName == null) return;
        Photo p = new Photo(photoName);
        if(!thisAlbum.photos.contains(p)) return;
        int index = thisAlbum.photos.indexOf(p);
        Photo photoToMove = thisAlbum.photos.get(index);
        thisAlbum.photos.remove(photoToMove);
        if((long)selectedAlbum <= albumToMoveToPosition){
            ++albumToMoveToPosition;
        }
        moveListener.movePhoto(photoToMove, albumToMoveToPosition);
        dismiss();
    }

    public interface MovePhotoDialogListener{
        void movePhoto(Photo p, long albumToMoveToPos);
    }
    public interface AddPhotoDialogListener{
        void addPhoto(Photo p);
    }
    public interface DeletePhotoDialogListener{
        void deletePhoto(Photo p);
    }

}
