package com.example.photos;

import android.app.ActionBar;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ListView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class PhotoViewingActivity extends AppCompatActivity implements PhotoActivityDialog.MovePhotoDialogListener, PhotoActivityDialog.AddPhotoDialogListener,
PhotoActivityDialog.DeletePhotoDialogListener{

    Button addPhotoBtn;
    Button movePhotoBtn;
    Button deletePhotoBtn;
    Button slideShowBtn;
    ListView photoListView;
    ArrayList<Photo> photos;
    ArrayList<Album> albums;
    int selectedAlbum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_viewing);
        if(getIntent().hasExtra("albums") && getIntent().hasExtra("selectedPos")){
            albums = (ArrayList<Album>)getIntent().getSerializableExtra("albums");
            photos = albums.get(getIntent().getIntExtra("selectedPos", 0)).photos;
            selectedAlbum = getIntent().getIntExtra("selectedPos", 0);
        } else{
            albums = new ArrayList<Album>();
            photos = new ArrayList<Photo>();
            selectedAlbum = 0;
        }
        slideShowBtn = findViewById(R.id.slideShowBtn);
        slideShowBtn.setOnClickListener((v)->slideShow());
        addPhotoBtn = findViewById(R.id.addPhotoBtn);
        addPhotoBtn.setOnClickListener((v)->addPhoto());
        movePhotoBtn = findViewById(R.id.movePhotoBtn);
        movePhotoBtn.setOnClickListener((v)->movePhoto());
        deletePhotoBtn = findViewById(R.id.deletePhotoBtn);
        deletePhotoBtn.setOnClickListener((v)->deletePhoto());
        photoListView = findViewById(R.id.photoListView);
        photoListView.setOnItemClickListener(((parent, view, position, id) -> display(position)));
        PhotoAdapter pA = new PhotoAdapter(getApplicationContext(), photos);
        photoListView.setAdapter(pA);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(this, AlbumViewingActvitiy.class);
        intent.putExtra("albums", albums);
        startActivity(intent);
        return true;
    }

    private void display(int position){
        Intent intent = new Intent(this, DisplayActivity.class);
        intent.putExtra("albums", albums);
        intent.putExtra("photos", photos);
        intent.putExtra("selectedPos", selectedAlbum);
        intent.putExtra("selected photo", position);
        startActivity(intent);
        PhotoAdapter pA = new PhotoAdapter(getApplicationContext(), photos);
        photoListView.setAdapter(pA);
    }

    private void addPhoto(){
        AppCompatDialogFragment addPhotoDialog = new PhotoActivityDialog();
        Bundle args = new Bundle();
        args.putString("operation", "add photo");
        args.putSerializable("albums", albums);
        addPhotoDialog.setArguments(args);
        addPhotoDialog.show(getSupportFragmentManager(), "Input Photo Path");
    }

    private void deletePhoto(){
        AppCompatDialogFragment deletePhotoDialog = new PhotoActivityDialog();
        Bundle args = new Bundle();
        args.putString("operation", "delete photo");
        args.putSerializable("albums", albums);
        deletePhotoDialog.setArguments(args);
        deletePhotoDialog.show(getSupportFragmentManager(), "Input Photo Name");
    }

    private void movePhoto(){
        AppCompatDialogFragment movePhotoDialog = new PhotoActivityDialog();
        Bundle args = new Bundle();
        args.putString("operation", "move photo");
        args.putSerializable("albums", albums);
        args.putInt("selected album", selectedAlbum);
        movePhotoDialog.setArguments(args);
        movePhotoDialog.show(getSupportFragmentManager(), "Move Photo");
    }

    private void slideShow(){
        if(photos.size() == 0) return;
        Intent intent = new Intent(this, SlideShowActivity.class);
        intent.putExtra("albums", albums);
        intent.putExtra("photos", photos);
        intent.putExtra("selectedPos", selectedAlbum);
        startActivity(intent);
    }

    @Override
    public void movePhoto(Photo p, long albumToMoveToPos) {
        Album toMoveTo = albums.get((int)albumToMoveToPos);
        if(!toMoveTo.photos.contains(p)){
            albums.get((int)albumToMoveToPos).photos.add(p);
        }
        PhotoAdapter pA = new PhotoAdapter(getApplicationContext(), photos);
        photoListView.setAdapter(pA);
    }

    @Override
    public void deletePhoto(Photo p) {
        if(photos.contains(p)){
            photos.remove(p);
        }
        PhotoAdapter pA = new PhotoAdapter(getApplicationContext(), photos);
        photoListView.setAdapter(pA);
    }

    @Override
    public void addPhoto(Photo p) {
        File file = new File(p.filePath);
        if((file.exists()) && (!photos.contains(p))){
            photos.add(p);
        }
        PhotoAdapter pA = new PhotoAdapter(getApplicationContext(), photos);
        photoListView.setAdapter(pA);
    }

    @Override
    protected void onStop(){
        super.onStop();
        FileOutputStream fos = null;
        try {
            fos = openFileOutput("user.dat", MODE_PRIVATE);
        } catch (FileNotFoundException e) {
            File file = new File(getFilesDir(), "user.dat");
            try {
                file.createNewFile();
            } catch (IOException e1) {

            }
        }
        ObjectOutputStream oos = null;
        try{
            oos = new ObjectOutputStream(fos);
            oos.writeObject(albums);
        } catch(Exception e){
            return;
        }
    }
}
