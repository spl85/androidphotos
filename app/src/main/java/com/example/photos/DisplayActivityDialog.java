package com.example.photos;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class DisplayActivityDialog extends AppCompatDialogFragment {

    AddTagListener addTagListener;
    DeleteTagListener deleteTagListener;
    EditText tagVal;
    RadioButton locationRadioBtn;
    RadioButton personRadioBtn;
    String operation;
    RadioGroup group;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        addTagListener = (AddTagListener)context;
        deleteTagListener = (DeleteTagListener)context;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = null;
        if(getArguments() != null){
            operation = getArguments().getString("operation");
        }

        LayoutInflater inflater = getActivity().getLayoutInflater();
        view = inflater.inflate(R.layout.add_tag_dialog, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setNegativeButton("cancel", ((dialog, which) -> {}));
        builder.setPositiveButton("ok", ((dialog, which) -> okClick()));
        tagVal = view.findViewById(R.id.tagValEditText);
        locationRadioBtn = view.findViewById(R.id.locationRadioBtn);
        personRadioBtn = view.findViewById(R.id.personRadioBtn);
        group = view.findViewById(R.id.radioGroup);
        locationRadioBtn.toggle();
        builder.setView(view);
        return builder.create();
    }

    private void okClick(){
        if(operation.equals("add tag")){
            String tagType;
            if(group.getCheckedRadioButtonId() == R.id.locationRadioBtn){
                tagType = "location";
            } else{
                tagType = "person";
            }
            String tagVal = this.tagVal.getText().toString();
            Tag t = new Tag(tagType, tagVal);
            addTagListener.addTag(t);
        }
        if(operation.equals("delete tag")){
            String tagType;
            if(group.getCheckedRadioButtonId() == R.id.locationRadioBtn){
                tagType = "location";
            } else{
                tagType = "person";
            }
            String tagVal = this.tagVal.getText().toString();
            Tag t = new Tag(tagType, tagVal);
            deleteTagListener.deleteTag(t);
        }
    }

    public interface AddTagListener{
        void addTag(Tag t);
    }

    public interface DeleteTagListener{
        void deleteTag(Tag t);
    }
}
