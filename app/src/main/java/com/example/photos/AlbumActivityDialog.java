package com.example.photos;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

public class AlbumActivityDialog extends AppCompatDialogFragment{

    private AddAlbumDialogListener addListener;
    private DeleteAlbumDialogListener deleteListener;
    private RenameAlbumListener renameListener;
    private EditText albumNameText;
    private EditText oldAlbumNameText;
    private EditText newAlbumNameText;
    private String mode;

    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState){
        if(getArguments() != null){
            mode = getArguments().getString("operation");
        }
        if(mode == null) return null;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = null;
        if(mode.equals("add album") || mode.equals("delete album")) {
            view = inflater.inflate(R.layout.add_album_dialog, null);
            albumNameText = view.findViewById(R.id.albumNameText);
        } else{
            view = inflater.inflate(R.layout.rename_album_dialog, null);
            oldAlbumNameText = view.findViewById(R.id.oldAlbumName);
            newAlbumNameText = view.findViewById(R.id.newAlbumNameText);
        }
        builder.setView(view);
        builder.setTitle("Input Album Name");
        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(mode.equals("add album")) {
                    if (!albumNameText.getText().toString().equals("")) {
                        Album album = new Album(albumNameText.getText().toString());
                        addListener.addAlbum(album);
                    }
                }
                if(mode.equals("delete album")){
                    if(!albumNameText.getText().equals("")){
                        Album toDelete = new Album(albumNameText.getText().toString());
                        deleteListener.deleteAlbum(toDelete);
                    }
                }
                if(mode.equals("rename album")){
                    if(!(oldAlbumNameText.getText().equals("") || newAlbumNameText.getText().equals(""))){
                        Album oldAlbum = new Album(oldAlbumNameText.getText().toString());
                        String newName = newAlbumNameText.getText().toString();
                        renameListener.renameAlbum(oldAlbum, newName);
                    }
                }
            }
        });
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        addListener = (AddAlbumDialogListener)context;
        deleteListener = (DeleteAlbumDialogListener) context;
        renameListener = (RenameAlbumListener)context;

    }

    public interface AddAlbumDialogListener{
        void addAlbum(Album a);
    }

    public interface DeleteAlbumDialogListener{
        void deleteAlbum(Album a);
    }

    public interface RenameAlbumListener{
        void renameAlbum(Album toRename, String newName);
    }
}
