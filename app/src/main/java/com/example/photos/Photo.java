package com.example.photos;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Photo class aggregates tags
 * A photo's date is generated externally with respect to this class from a file's last modification date,
 * and the image of the file is loaded externally in controllers when needed
 * using the filepath to the image on the user's personal computer
 *  */
public class Photo implements Serializable {

    /** Absolute path of the image */
    public String filePath;

    /** Date the photo was created */
    public Date date;

    /** Photo's caption */
    public String caption;

    /** Photo's tags */
    public ArrayList<Tag> tags;


    public Photo(String name){
        this.caption = name;
        this.date = null;
        this.tags = new ArrayList<Tag>();
        this.filePath = "";
    }

    public Photo(String filePath, String name){
        this.filePath = filePath;
        caption = name;
        this.date = null;
        this.tags = new ArrayList<>();
    }

    /**
     * Creates a new Photo object with an image path, date, and caption
     * @param filePath Path in absolute terms to the image on the user's computer
     * @param date Date the photo was taken
     */
    public Photo(String filePath, long date){
        this.filePath = filePath;
        this.date = new Date(date);
        this.caption = "";
        this.tags = new ArrayList<Tag>();
    }


    /**
     * Creates a new Photo object with an image path, date, caption, and starting set of tags
     * @param filePath Path in absolute terms to the image on the user's computer
     * @param date Date the photo was taken
     * @param caption Caption of the photo
     * @param tags Set of tags for the photo
     */
    public Photo(String filePath, long date, String caption, ArrayList<Tag> tags){
        this.filePath = filePath;
        this.date = new Date(date);
        this.caption = caption;
        if(tags == null) {
            this.tags = new ArrayList<Tag>();
        } else{
            this.tags = tags;
        }
    }

    /**
     * Get a human readable, and displayable date of the photo
     * @return Human readable date that photo was last modified
     */
    public String getHRDate(){
        return new SimpleDateFormat("MM/dd/yy").format(this.date);
    }

    /**
     * Returns the tags currently on photo
     * @return Tags currently on the photo
     */
    public ArrayList<Tag> getAllTags(){
        return this.tags;
    }

    /**
     * Adds a tag to this photo
     * @param t Tag to be added
     * @return True if the tag was added, otherwise returns false if the photo is already tagged with 't'
     */
    public boolean addTag(Tag t){
        if(this.tags.contains(t)) return false;
        return this.tags.add(t);
    }

    /**
     * Checks whether this photos has the tag pointed to by t
     * @param t Tag to check for
     * @return Returns true if photo is tagged with t, otherwise false
     */
    public boolean hasTag(Tag t){
        for(Tag tag: this.tags)
        {
            if(tag.equals(t)){
                return true;
            }
        }
        return false;
    }

    /**
     * Removes tag from current photo tags
     * @param t Tag to remove
     * @return True if the tag removal was successful, otherwise returns false
     */
    public boolean removeTag(Tag t){
        return this.tags.remove(t);
    }

    /**
     * Checks for equality between an object and this photo
     * @param o Object to be compared
     * @return  True if photos are the same; false otherwise
     */
    public boolean equals(Object o){
        if(!(o instanceof Photo)) return false;

        Photo p = (Photo)o;
        //System.out.println(" we are grabbing " + this.caption.substring(this.caption.lastIndexOf('/') + 1));
        String trimmedString = this.caption.substring(this.caption.lastIndexOf('/') + 1);
        if(p.caption.equals(trimmedString)) return true;
        return false;

    }
}
