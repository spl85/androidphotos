package com.example.photos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Representation of a User on the system
 * A user can search for photos using one tag, or perform a conjunctive or disjunctive search using two tags
 */
public class User implements Serializable {

    /** User's unique username */
    public String username;

    /** User's unique file path containing serialized data for this user */
    public String serialPath;

    /** User's albums */
    public ArrayList<Album> albums;

    /**
     * Creates a new User
     * @param username User's unique username
     * @param serialPath User's unique file path containing serialized data for this user
     */
    public User(String username, String serialPath){
        this.username = username;
        this.serialPath = serialPath;
        this.albums = new ArrayList<Album>();
    }

    /**
     * Get's user's albums
     * @return User's current albums
     */
    public List<Album> getAlbums(){
        return this.albums;
    }

    /**
     * Adds an album to the user's collection
     * @param a Album to add to user's collection
     * @return True if add was successful, otherwise false if the album already exists in the user's collection
     */
    public boolean addAlbum(Album a){
        if(albums.contains(a)) return false;
        albums.add(a);
        return true;
    }

    /**
     * Removes an album in the user's collection, if present
     * @param a Album to remove
     * @return True if the remove was successful, otherwise returns false if the album is not in the user's collection
     */
    public boolean removeAlbum(Album a){
        if(!this.albums.contains(a)) return false;
        return this.albums.remove(a);
    }

    /**
     * Searches the entire user's collection of albums for photos tagged with t, creates a new album, and adds it to the user's collection
     * @param t Tag parameter to search by for photos
     * @return True if the single tag search added the resulting album to the collection, otherwise returns false
     */
    public Album singleTagSearch(Tag t){
        Album searchResults = new Album("Search: " + t.toString());
        for(Album a : this.albums){
            searchResults.addAllPhotos(a.singleTagSearch(t));
        }
        return searchResults;
    }

    /**
     * Searches the entire user's collection of albums for photos tagged with t1 and/or t2, creates a new album, and adds it to the user's collection with name
     * "Search: t1 AND/OR t2" e.g. "Search: person=sesh AND location=prague"
     * @param t1 First tag parameter
     * @param t2 Second tag parameter
     * @param mode Mode is "and" for conjunctive, and "or" for disjunctive
     * @return True if the double tag search added the resulting album to the collection, otherwise returns false
     */
    public Album doubleTagSearch(Tag t1, Tag t2, String mode){
        Album searchResults = new Album("Tag Search: " + t1.toString() + " " + mode.toUpperCase() + " " + t2.toString());
        for(Album a: this.albums){
            searchResults.addAllPhotos(a.doubleTagSearch(t1, t2, mode));
        }
        return searchResults;
    }

    /**
     * Converts this User object into a string representation
     * @return String representation of this User object
     */
    public String toString(){
        return this.username;
    }

    /**
     * Checks whether Object pointed to by 'o' is equal to this User
     * @param o Object to compare with
     * @return True if Object o is this user, otherwise returns false
     */
    public boolean equals(Object o){
        if(!(o instanceof User)) return false;

        User toCompare = (User)o;

        if(toCompare.toString().equals(this.toString())) return true;
        return false;
    }
}
