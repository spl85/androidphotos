package com.example.photos;

import java.io.Serializable;

/**
 * Representation of a tag of a photo
 * Has tag type, tag value pair
 */
public class Tag implements Serializable {

    /** Type of the tag */
    public String tagType;

    /** Value for this tag */
    public String tagValue;

    public Tag(String tagType, String tagValue){
        this.tagType = tagType;
        this.tagValue = tagValue;
    }

    /**
     * Turns tag object into a readable string
     * @return Representation of a Tag in String format
     */
    public String toString(){
        return this.tagType + "=" + this.tagValue;
    }

    /**
     * Checks for equality between an object and this tag
     * @param o Object to be compared
     * @return  True if tags are the same; false otherwise.
     */
    public boolean equals(Object o){
        if(!(o instanceof Tag)) return false;

        Tag toCompare = (Tag)o;
        if(toCompare.toString().equals("location=") || toCompare.toString().equals("person=")) return false;
        if(this.toString().contains(toCompare.toString())) return true;
        return false;
    }

}
