package com.example.photos;

import android.content.Context;
import android.graphics.ImageDecoder;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class PhotoAdapter extends BaseAdapter {

    private ArrayList<Photo> photos;
    private LayoutInflater mLI;
    private Drawable drawable;
    private File imageFile;

    public PhotoAdapter(Context context, ArrayList<Photo> photos){
        this.photos = photos;
        mLI = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return photos.size();
    }

    @Override
    public Object getItem(int position) {
        return photos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = mLI.inflate(R.layout.photo_cell, null);
        ImageView photoImage = view.findViewById(R.id.photoImage);
        TextView photoCaption = view.findViewById(R.id.photoCaption);
        photoCaption.setText(photos.get(position).caption);
        imageFile = new File(photos.get(position).filePath);
        ImageDecoder.Source source = ImageDecoder.createSource(imageFile);
        try {
            drawable = ImageDecoder.decodeDrawable(source);
        } catch (IOException e) {
        }
        photoImage.setImageDrawable(drawable);
        return view;
    }

}
