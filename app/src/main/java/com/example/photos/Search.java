package com.example.photos;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class Search extends AppCompatActivity {
    RadioGroup radioGroup;
    RadioButton radioButton;
    EditText searchPersonTagText;
    EditText searchLocationTagText;
    String choice = "";
    User user;
    Album toCreate;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        user = new User("user", "x");
        user.albums = (ArrayList<Album>)getIntent().getSerializableExtra("albums");
        radioGroup = findViewById(R.id.radioGroup);
        searchPersonTagText = findViewById(R.id.personSearchTag);
        searchLocationTagText = findViewById(R.id.locationSearch);
        toCreate = new Album("default");
        Button searchButtonApply = findViewById(R.id.searchButton);
        searchButtonApply.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                int radioId = radioGroup.getCheckedRadioButtonId();
                radioButton = findViewById(radioId);
                String sPerson = searchPersonTagText.getText().toString();
                String sLocation = searchLocationTagText.getText().toString();
                if(sPerson.equals("") && sLocation.equals("")) return;
                if (radioId == R.id.andRadioButton) {
                    choice = "AND";
                }
                else if (radioId == R.id.orRadioButton){
                    choice = "OR";
                }
                if(!(sPerson.equals("") || sLocation.equals("")) && !choice.equals("")){
                    //perform double tag search
                    Tag tag1 = new Tag("person", sPerson);
                    Tag tag2 = new Tag("location", sLocation);
                    toCreate = user.doubleTagSearch(tag1, tag2, choice);
                } else if(radioId == R.id.locationOnlySearchRadioButton){
                    Tag t = new Tag("location", sLocation);
                    toCreate = user.singleTagSearch(t);
                } else if(radioId == R.id.personOnlySearchRadioButton){
                    Tag t = new Tag("person", sPerson);
                    toCreate = user.singleTagSearch(t);
                }

                if (toCreate.photos.isEmpty()){
                    String alert = "No results found";
                    Toast.makeText(Search.this, "No results found", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(getApplicationContext(), AlbumViewingActvitiy.class);
                intent.putExtra("new album", toCreate);
                startActivity(intent);
            }
        });
    }


}
